# eventsourced Kafka event publisher

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/kafka)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/kafka) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/kafka?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/kafka) [![Build Status](https://gitlab.com/unboundsoftware/eventsourced/kafka/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/kafka/commits/main)[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/kafka/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/kafka/commits/main)

Package `kafka` provides a [Kafka](https://kafka.apache.org/) implementation of the event publisher which can be used with the [eventsourced framework](https://gitlab.com/unboundsoftware/eventsourced/eventsourced).

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/kafka
```
