package kafka

import (
	"context"
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/sanity-io/litter"
	"gitlab.com/unboundsoftware/go-kafka"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

func TestKafka_Publish(t *testing.T) {
	type fields struct {
		client kafka.Client
		topic  string
	}
	type args struct {
		event eventsourced.Event
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "return error from Kafka",
			fields: fields{
				client: &mockKafka{send: func(topic string, msg interface{}) error {
					if topic != "some.topic" {
						t.Errorf("Publish() got %s, want %s", topic, "some.topic")
					}
					want := &TestEvent{
						ID:   "abc",
						Name: "Nisse",
					}
					if !reflect.DeepEqual(msg, want) {
						t.Errorf("Publish() got %s, want %s", litter.Sdump(msg), litter.Sdump(want))
					}
					return errors.New("error")
				}},
				topic: "some.topic",
			},
			args: args{event: &TestEvent{
				ID:   "abc",
				Name: "Nisse",
			}},
			wantErr: true,
		},
		{
			name: "return nil on success",
			fields: fields{
				client: &mockKafka{send: func(topic string, msg interface{}) error {
					return nil
				}},
				topic: "some.topic",
			},
			args: args{event: &TestEvent{
				ID:   "abc",
				Name: "Nisse",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &Kafka{
				client: tt.fields.client,
				topic:  tt.fields.topic,
			}
			if err := k.Publish(context.Background(), tt.args.event); (err != nil) != tt.wantErr {
				t.Errorf("Publish() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		topic  string
		client kafka.Client
	}
	tests := []struct {
		name string
		args args
		want *Kafka
	}{
		{
			name: "return an instance populated with provided values",
			args: args{
				topic:  "some.topic",
				client: &mockKafka{},
			},
			want: &Kafka{
				client: &mockKafka{},
				topic:  "some.topic",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args.topic, tt.args.client); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

type mockKafka struct {
	send func(topic string, msg interface{}) error
}

func (m mockKafka) Send(topic string, msg interface{}) error {
	return m.send(topic, msg)
}

func (m mockKafka) Consume(map[string]func(msg []byte) error) error {
	panic("implement me")
}

func (m mockKafka) Close() {
	panic("implement me")
}

var _ kafka.Client = &mockKafka{}

type TestEvent struct {
	ID   string
	Name string
}

func (e TestEvent) AggregateIdentity() eventsourced.ID {
	panic("implement me")
}

func (e TestEvent) SetAggregateIdentity(_ eventsourced.ID) {
}

func (e TestEvent) When() time.Time {
	panic("implement me")
}

func (e TestEvent) SetWhen(_ time.Time) {
	panic("implement me")
}

func (e TestEvent) GlobalSequenceNo() int {
	//TODO implement me
	panic("implement me")
}

func (e TestEvent) SetGlobalSequenceNo(seqNo int) {
	//TODO implement me
	panic("implement me")
}

var _ eventsourced.Event = &TestEvent{}
