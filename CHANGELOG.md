# Changelog

All notable changes to this project will be documented in this file.

## [0.1.0] - 2024-11-27

### 🚀 Features

- Initial commit
- [**breaking**] Add context to fulfill eventsourced contract

### 🐛 Bug Fixes

- *(deps)* Update module gitlab.com/unboundsoftware/eventsourced/eventsourced to v1.15.0

### ⚙️ Miscellaneous Tasks

- Add dependabot config
- Remove dependabot-standalone
- Change to codecov binary instead of bash uploader
- Replace golint with golangci-lint and run as separate step
- Add vulnerability-check
- Remove Stop
- Change default branch to main
- Switch to manual rebases for Dependabot
- Update to golang 1.20.1
- Update to golangci-lint 1.51.2
- Update to Go 1.20.3
- *(deps)* Update to Go 1.20.5
- Update Go and golangci-lint versions
- Use go 1.21.4
- Update version of Go and golangci-lint
- Add release flow

<!-- generated by git-cliff -->
