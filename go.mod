module gitlab.com/unboundsoftware/eventsourced/kafka

go 1.15

require (
	github.com/sanity-io/litter v1.5.8
	gitlab.com/unboundsoftware/eventsourced/eventsourced v1.18.1
	gitlab.com/unboundsoftware/go-kafka v0.0.2
)
